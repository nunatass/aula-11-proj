package test.java.pt.uevora;

import main.java.pt.uevora.MyCalculator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.*;

public class MyCalculatorTest {

    MyCalculator calculator;

    @Before
    public void setUp(){
        calculator = new MyCalculator();
    }

    @After
    public void down(){
        calculator = null;
    }


    @Test
    public void testSum() throws Exception {
        Double result = calculator.execute("2+3");

        assertEquals("The sum result of 2 + 3 must be 5",  5D, (Object)result);
    }


    @Test
    public void testSub() throws Exception {
        Double result = calculator.execute("2-3");

        assertEquals("The subtraction result of 2 - 3 must be -1",  -1D, (Object)result);
    }


    @Test
    public void testMul() throws Exception {
        Double result = calculator.execute("2*3");

        assertEquals("The multiplication result of 2 * 3 must be 6",  6D, (Object)result);
    }

    @Test
    public void testDiv() throws Exception {
        Double result = calculator.execute("3/2");

        assertEquals("The division result of 3 / 2 must be ",  1.5D, (Object)result);
    }


    

}